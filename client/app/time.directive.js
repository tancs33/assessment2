(function () {
  angular.module('EMSApp')
      .directive("myCurrentTime", myCurrentTime);
          function myCurrentTime(dateFilter){
            var format;
            return (scope, element, attrs)=>{
                
                scope.$watch(attrs.myCurrentTime, function(value) {
                    console.log(value);
                    format = value;
                    updateTime();
                });
    
                function updateTime(){
                    var dt = dateFilter(new Date(), format);
                    element.text(dt);
                }
    
                function updateLater() {
                    setTimeout(function() {
                        updateTime(); // update DOM
                        updateLater(); // schedule another update
                    }, 1000);
                }
    
                updateLater();
            }
      }
  }());