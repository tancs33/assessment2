(function () {
    angular
        .module("EMSApp")
        .controller("GroceryController", GroceryController)
        .controller("EditGroceryCtrl", EditGroceryCtrl)
        .controller("AddGroceryCtrl", AddGroceryCtrl);
       // .controller("DeleteGroceryCtrl", DeleteGroceryCtrl)
        
    GroceryController.$inject = ['EMSAppAPI', '$uibModal', '$document', '$scope'];
    EditGroceryCtrl.$inject = ['$uibModalInstance', 'EMSAppAPI', 'items', '$rootScope', '$scope'];
    AddGroceryCtrl.$inject = ['$uibModalInstance', 'EMSAppAPI', 'items', '$rootScope', '$scope'];
    DeleteGroceryCtrl.$inject = ['$uibModalInstance', 'EMSAppAPI', 'items', '$rootScope', '$scope'];
    
      function DeleteGroceryCtrl($uibModalInstance, EMSAppAPI, items, $rootScope, $scope){
        var self = this;
        //self.items = items;
        self.deleteGrocery = deleteGrocery;
        console.log(items);
        EMSAppAPI.getGrocery(items).then((result)=>{
            console.log(result.data);
            self.grocery =  result.data;
            /*self.grocery.birth_date = new Date( self.grocery.birth_date);
            self.egrocery.hire_date = new Date( self.grocery.hire_date);
            console.log(self.Grocery.birth_date);*/
        });

        function deleteGrocery(){
            console.log("delete employee ...");
            EMSAppAPI.deleteGrocery(self.grocery.id).then((result)=>{
                console.log(result);
                $rootScope.$broadcast('refreshGroceryList');
                $uibModalInstance.close(self.run);
            }).catch((error)=>{
                console.log(error);
            });
        }

    } 
//ADD grocery
    function AddGroceryCtrl($uibModalInstance, EMSAppAPI, items, $rootScope, $scope){
        console.log("Add Grocery");
        var self = this;
        self.saveGrocery = saveGrocery;

        self.grocery = {
            gender: "M"
        }
        initializeCalendar($scope);
        function saveGrocery(){
            console.log("save grocery ...");
            console.log(self.grocery.name);
            console.log(self.grocery.brand);
            console.log(self.grocery.upc12);
            
            EMSAppAPI.addGrocery(self.grocery).then((result)=>{
                //console.log(result);
                console.log("Addgrocery -> " + result.id);
                $rootScope.$broadcast('refreshGroceryFromAdd', result.data);
             }).catch((error)=>{
                console.log(error);
                self.errorMessage = error;
             })
            $uibModalInstance.close(self.run);
        }
    }
        
    function initializeCalendar($scope){
        self.datePattern = /^\d{4}-\d{2}-\d{2}$/;;
        
        $scope.today = function() {
            $scope.dt = new Date();
        };
        $scope.today();
    
        $scope.clear = function() {
            $scope.dt = null;
        };
    
        $scope.inlineOptions = {
            customClass: getDayClass,
            minDate: new Date(),
            showWeeks: true
        };
    
        $scope.dateOptions = {
            //dateDisabled: disabled,
            formatYear: 'yy',
            maxDate: new Date(2020, 5, 22),
            minDate: new Date(),
            startingDay: 1
        };

        // Disable weekend selection
        function disabled(data) {
            console.log(data);
            var date = data.date,
                mode = data.mode;
            return mode === 'day' && (date.getDay() === 0 || date.getDay() === 6);
        }
    
        $scope.toggleMin = function() {
        $scope.inlineOptions.minDate = $scope.inlineOptions.minDate ? null : new Date();
        $scope.dateOptions.minDate = $scope.inlineOptions.minDate;
        };
    
        $scope.toggleMin();
    
        $scope.open1 = function() {
            $scope.popup1.opened = true;
        };

        $scope.open2 = function() {
            $scope.popup2.opened = true;
        };
    
        $scope.setDate = function(year, month, day) {
            $scope.dt = new Date(year, month, day);
        };
    
        $scope.formats = ['dd-MMMM-yyyy', 'yyyy-MM-dd', 'dd.MM.yyyy', 'shortDate'];
        $scope.format = $scope.formats[1];
        $scope.altInputFormats = ['M!/d!/yyyy'];
    
        $scope.popup1 = {
            opened: false
        };

        $scope.popup2 = {
            opened: false
        };
    
        var tomorrow = new Date();
        tomorrow.setDate(tomorrow.getDate() + 1);
        var afterTomorrow = new Date();
        afterTomorrow.setDate(tomorrow.getDate() + 1);
        $scope.events = [
        {
            date: tomorrow,
            status: 'full'
        },
        {
            date: afterTomorrow,
            status: 'partially'
        }
        ];
    
        function getDayClass(data) {
            var date = data.date,
                mode = data.mode;
            if (mode === 'day') {
                var dayToCheck = new Date(date).setHours(0,0,0,0);
        
                for (var i = 0; i < $scope.events.length; i++) {
                var currentDay = new Date($scope.events[i].date).setHours(0,0,0,0);
        
                if (dayToCheck === currentDay) {
                    return $scope.events[i].status;
                }
                }
            }
        
            return '';
        }
   }

    function EditGroceryCtrl($uibModalInstance, EMSAppAPI, items, $rootScope, $scope){
        console.log("Edit Grocery Ctrl");
        var self = this;
        self.items = items;
        initializeCalendar($scope);

        EMSAppAPI.getGrocery(items).then((result)=>{
           console.log(result.data);
           self.grocery =  result.data;
          /* self.employee.birth_date = new Date( self.employee.birth_date);
           self.employee.hire_date = new Date( self.employee.hire_date);
           console.log(self.employee.birth_date);*/
        })

        self.saveGrocery = saveGrocery;

        function saveGrocery(){
            console.log("save grocery ...");
            console.log(self.grocery.name);
            console.log(self.grocery.brand);
            console.log(self.grocery.upc12);
          /*  console.log(self.employee.hire_date);
            console.log(self.employee.birth_date);
            console.log(self.employee.gender); */
            EMSAppAPI.updateGrocery(self.grocery).then((result)=>{
                console.log(result);
                $rootScope.$broadcast('refreshGroceryList');
             }).catch((error)=>{
                console.log(error);
             })
            $uibModalInstance.close(self.run);
        }

    }

    function GroceryController(EMSAppAPI, $uibModal, $document, $scope) {
        var self = this;
        self.format = "M/d/yy h:mm:ss a";
        
        self.grocery = [];
        self.maxsize=5;
        self.totalItems = 0;
        self.itemsPerPage = 20;
        self.currentPage = 1;

        self.searchGrocery =  searchGrocery;
        self.addGrocery =  addGrocery;
        self.editGrocery = editGrocery;
        self.deleteGrocery = deleteGrocery;
        self.pageChanged = pageChanged;

        function searchAllGrocery(searchKeyword,orderby,itemsPerPage,currentPage){
            EMSAppAPI.searchGrocery(searchKeyword, orderby, itemsPerPage, currentPage).then((results)=>{
                self.grocery = results.data.rows;
                self.totalItems = results.data.count;
                //$scope.numPages = Math.ceil(self.totalItems /self.itemsPerPage);
            }).catch((error)=>{
                console.log(error);
            });
        }


        function pageChanged(){
            console.log("Page changed " + self.currentPage);
            searchAllGrocery(self.searchKeyword, self.orderby,self.itemsPerPage, self.currentPage);
            console.log($scope.numPages);
        }

        $scope.$on("refreshGroceryList",function(){
            console.log("refresh grocery list "+ self.searchKeyword);
            searchAllGrocery(self.searchKeyword, self.orderby,self.itemsPerPage, self.currentPage);
        });

        $scope.$on("refreshGroceryListFromAdd",function(event, args){
            console.log("refresh grocery list from id"+ args.id);
            var grocery = [];
            grocery.push(args);
            self.searchKeyword = "";
            self.grocery = grocery;
        });

        function searchGrocery(){
            console.log("search Grocery  ....");
            console.log(self.orderby);
            searchAllGrocery(self.searchKeyword, self.orderby,self.itemsPerPage, self.currentPage);
        }

        function addGrocery(size, parentSelector){
            console.log("post add Grocery  ....");
            var items = [];
            var parentElem = parentSelector ? 
                angular.element($document[0].querySelector('.modal-demo ' + parentSelector)) : undefined;
            var modalInstance = $uibModal.open({
                animation: self.animationsEnabled,
                ariaLabelledBy: 'modal-title',
                ariaDescribedBy: 'modal-body',
                templateUrl: 'addGrocery.html',
                controller: 'AddGroceryCtrl',
                controllerAs: 'ctrl',
                size: size,
                appendTo: parentElem,
                resolve: {
                    items: function () {
                        return items;
                    }
                }
            }).result.catch(function (resp) {
                if (['cancel', 'backdrop click', 'escape key press'].indexOf(resp) === -1)      throw resp;
            });
            
        }

        function editGrocery(id, size, parentSelector){
            console.log("Edit Grocery...");
            console.log("id no > " + id);
            
            var parentElem = parentSelector ? 
            angular.element($document[0].querySelector('.modal-demo ' + parentSelector)) : undefined;
            var modalInstance = $uibModal.open({
                animation: self.animationsEnabled,
                ariaLabelledBy: 'modal-title',
                ariaDescribedBy: 'modal-body',
                templateUrl: 'editGrocery.html',
                controller: 'EditGroceryCtrl',
                controllerAs: 'ctrl',
                size: size,
                appendTo: parentElem,
                resolve: {
                    items: function () {
                        return id;
                    }
                }
            }).result.catch(function (resp) {
                if (['cancel', 'backdrop click', 'escape key press'].indexOf(resp) === -1)      throw resp;
            });
        }

        function deleteGrocery(id, size, parentSelector){
            console.log("delete Grocery...");
            console.log("id_no > " + id);
            
            var parentElem = parentSelector ? 
            angular.element($document[0].querySelector('.modal-demo ' + parentSelector)) : undefined;
            var modalInstance = $uibModal.open({
                animation: self.animationsEnabled,
                ariaLabelledBy: 'modal-title',
                ariaDescribedBy: 'modal-body',
                templateUrl: 'deleteGrocery.html',
                controller: 'DeleteGroceryCtrl',
                controllerAs: 'ctrl',
                size: size,
                appendTo: parentElem,
                resolve: {
                    items: function () {
                        return id;
                    }
                }
            }).result.catch(function (resp) {
                if (['cancel', 'backdrop click', 'escape key press'].indexOf(resp) === -1)      throw resp;
            });
        }
    }
})();