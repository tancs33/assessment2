(function () {  
    
    angular
    .module("EMSApp")
    .config(uirouterAppConfig);
    uirouterAppConfig.$inject = ["$stateProvider","$urlRouterProvider"];
    
    
    function uirouterAppConfig($stateProvider, $urlRouterProvider){
    
    $stateProvider
        .state("home",{  
            url : "/home",
            templateUrl: "home.html",
            controller : 'GroceryController',  
            controllerAs : 'ctrl'
        })
        .state("add", {
            url: "/add",
            templateUrl: "addGrocery.html",
            controller : 'GroceryController',
            controllerAs : 'ctrl'
        })

        .state("edit", {
            url: "/edit",
            templateUrl: "editGrocery.html",
            controller : 'GroceryController',
            controllerAs : 'ctrl'
        })

        .state("delete", {
            url: "/delete",
            templateUrl: "deleteGrocery.html",
            controller : 'GroceryController',
            controllerAs : 'ctrl'
        })
    
        $urlRouterProvider.otherwise("/home");  
    
    }
    
    })();