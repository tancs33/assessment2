var express = require("express");
var bodyParser = require('body-parser');
var Sequelize = require ("sequelize");
var moment = require('moment');
var offset = moment().utcOffset();
var finalOffset = ''.concat(offset < 0 ? "-" : "+",moment(''.concat(Math.abs(offset/60),Math.abs(offset%60) < 10 ? "0" : "",Math.abs(offset%60)),"hmm").format("HH:mm"));
var node_port = process.env.PORT || 3000;
const GROCERY_API = "/api/groceries";
//const mysql = require('mysql2');

//console.log(Sequelize);
const SQL_USERNAME="root";
const SQL_PASSWORD="vrujom"
var connection = new Sequelize(
    'grocery_list',
    SQL_USERNAME,
    SQL_PASSWORD,
    {
        host: 'localhost',
        port: 3306,
        logging: console.log,
        dialect: 'mysql',
        dialectOptions: {
            useUTC: false //for reading from database
        },
        timezone: finalOffset, //for writing to database
        pool: {
            max: 10,
            min: 0,
            idle: 20000,
            acquire: 20000
        }
    }
); 

var Grocery = require('./models/grocery_list')(connection, Sequelize);
const Op = Sequelize.Op
var app = express();
app.use(bodyParser.urlencoded({ limit: '50mb', extended: true }));
app.use(bodyParser.json({limit: '50mb'}));

app.use(express.static(__dirname + "/../client/"));

// create
app.post(GROCERY_API, (req, res)=>{
    console.log("..."+ JSON.stringify(req.body));
   // console.log("new upc12 number " + req.params.upc12); 
   //var upc12 = req.params.upc12;
//  var whereClause = {limit: 1, where: {upc12: upc12}};
   Grocery.create(grocery).then((result)=>{
        console.log(result);
        res.status(200).json(result);
    }).catch((error)=>{
        console.log(error);
        res.status(500).json(error);
    });
});

// update
app.put(GROCERY_API, (req, res)=>{
    console.log(">>> " + JSON.stringify(req.body));
    console.log("update grocery ..." + req.body);
    console.log(req.body.id);
    var id = req.body.id;
    console.log(id);
    var whereClause = {limit: 1, where: {id: id}};
    Grocery.findOne(whereClause).then((result)=>{
        result.update(req.body);
        res.status(200).json(result);
    }).catch((error)=>{
        console.log(error);
        res.status(500).json(error);
    });    
});

// retrieve 
app.get(GROCERY_API, (req, res)=>{
    console.log("search > " + req.query.keyword);
    var keyword = req.query.keyword;
    var sortby = req.query.sortby;
    var itemsPerPage = parseInt(req.query.itemsPerPage);
    var currentPage = parseInt(req.query.currentPage);
    var offset = (currentPage - 1) * itemsPerPage;
    console.log(offset);
    if(sortby == 'null'){
        console.log("sortby is null");
        sortby = "ASC";
    }
    console.log(keyword);
    console.log(itemsPerPage);
    console.log(currentPage);
    console.log(typeof offset);
    
    console.log("sortby " + sortby);
    console.log(typeof(keyword));
    if(keyword == ''){
        console.log("keyword is empty ?");
    }
    var whereClause = { offset: offset, limit: itemsPerPage, order: [['name', sortby], ['brand', sortby]]};
    console.log(keyword.length);
    console.log(keyword !== 'undefined');
    console.log(keyword != undefined);
    console.log(!keyword);
    console.log(keyword.trim().length > 0);

    if((keyword !== 'undefined' || !keyword) && keyword.trim().length > 0){
        console.log("> " + keyword);
        whereClause = { offset: offset, limit: itemsPerPage, order: [['name', sortby], ['brand', sortby]], where: {name: keyword}};
    }
    console.log(whereClause);
    Grocery.findAndCountAll(whereClause).then((results)=>{
        res.status(200).json(results);
    }).catch((error)=>{
        console.log(error);
        res.status(500).json(error);
    });    
});

app.get(GROCERY_API+"/:id", (req, res)=>{
    console.log("one grocery ...");
    console.log(req.params.id);
    var id = req.params.id;
    console.log(id);
    var whereClause = {limit: 1, where: {id: id}};
    Grocery.findOne(whereClause).then((result)=>{
        res.status(200).json(result);
    }).catch((error)=>{
        console.log(error);
        res.status(500).json(error);
    });    
});

app.delete(GROCERY_API+"/:id", (req, res)=>{
    console.log("one grocery ...");
    console.log(req.params.id);
    var id = req.params.id;
    console.log(id);
    var whereClause = {limit: 1, where: {id: id}};
    Grocery.findOne(whereClause).then((result)=>{
        result.destroy();
        res.status(200).json({});
    }).catch((error)=>{
        console.log(error);
        res.status(500).json(error);
    }); 
});

app.use(function (req, res) {
    res.send("<h1>Page not found</h1>");
});

app.listen(node_port, function () {
    console.log("Server running at http://localhost:" + node_port);
});

module.exports = app;