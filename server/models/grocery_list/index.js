module.exports = function(connection, Sequelize){
    // employees this have to match the mysql table
    // return Employees object is used within the JS
    var Grocery = connection.define('grocery_list', {
        id: {
            type: Sequelize.INTEGER(11),
            allowNull: false,
            primaryKey: true,
            autoIncrement: true
        },
        upc12: {
            type: Sequelize.BIGINT(12),
            allowNull: false
        },
        name: {
            type: Sequelize.STRING,
            allowNull: false
        },
        brand:{
            type: Sequelize.STRING,
            allowNull: false
        },
        /*gender:{
            type: Sequelize.ENUM('M', 'F'),
            allowNull: false 
        },
        hire_date: {
            type: Sequelize.DATE,
            allowNull: false 
        },*/
        photourl: {
            type: Sequelize.STRING,
            defaultValue: "http://www.barcodes4.me/barcode/c128a/035200264013.png?IsTextDrawn=1"
        }
    }, {
        timestamps: false,
        tableName: 'grocery_list'
    });
    return Grocery;
}